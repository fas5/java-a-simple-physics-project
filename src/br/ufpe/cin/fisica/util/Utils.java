package br.ufpe.cin.fisica.util;

import br.ufpe.cin.fisica.enums.Sentido;

/**
 * Metodos utilitarios utilizados por uma ou mais classes.
 *
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class Utils {

    /**
     * Escolhe o sentido para o qual a particula dara o passo.
     * 
     * @param randomizer Objeto que define a direcao para a qual a particula ira.
     * @return Sentido para o qual a particula dara o passo.
     */
    public static Sentido escolherSentido(Randomizer randomizer) {
        return Sentido.getSentidoById(randomizer.getGenerator().nextValue());
    }

    /**
     * Retorna o valor passado como o parametro incrementado em 1;
     * 
     * @param valor Valor que sera incrementado.
     * @return Valor incrementado em 1.
     */
    public static int incrementarValor(int valor) {
        return valor += 1;
    }

    /**
     * Retorna o valor passado como o parametro decrementado em 1;
     * 
     * @param valor Valor que sera decrementado.
     * @return Valor descrementado em 1.
     */
    public static int decrementarValor(int valor) {
        return valor -= 1;
    }
}
