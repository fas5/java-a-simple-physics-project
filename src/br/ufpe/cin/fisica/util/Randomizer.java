package br.ufpe.cin.fisica.util;

import java.util.Random;

import org.uncommons.maths.random.DiscreteUniformGenerator;

/**
 * Singleton utilizado pelos itens (a) e (b) para a escolha do sentido para o qual a particula dara o passo.
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class Randomizer {

    private static Randomizer instance;

    private DiscreteUniformGenerator generator;

    /**
     * Construtor padrao.
     */
    private Randomizer() {
        this.generator = new DiscreteUniformGenerator(0, 1, new Random());
    }

    /**
     * Retorna a instancia do objeto.
     * 
     * @return A instancia do objeto.
     */
    public static Randomizer getInstance() {
        if (instance == null) {
            return new Randomizer();
        } else {
            return instance;
        }
    }

    /**
     * Retorna o objeto que decide o sentido para o qual a particula dara o passo.
     * 
     * @return Objeto que decife o sentido para o qual a particula dara o passo.
     */
    public DiscreteUniformGenerator getGenerator() {
        return this.generator;
    }

}
