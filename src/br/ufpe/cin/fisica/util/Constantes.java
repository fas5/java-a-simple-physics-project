package br.ufpe.cin.fisica.util;

/**
 * Constantes utilizadas por uma ou mais classes.
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class Constantes {

    /**
     * Numero maximo de passos que serao executados. Utilizado no item (a).
     */
    public static final int PASSO_MAXIMO = 240;

    /**
     * Conjunto de passos maximos. Utilizados no item (b).
     */
    public static final int[] PASSOS_MAXIMOS = { 10, 30, 60, 120, 240 };

}
