package br.ufpe.cin.fisica;

import br.ufpe.cin.fisica.chart.GeradorGrafico;
import br.ufpe.cin.fisica.enums.Letra;
import br.ufpe.cin.fisica.questoes.PrimeiraQuestao;
import br.ufpe.cin.fisica.questoes.AbstractQuestao;
import br.ufpe.cin.fisica.questoes.SegundaQuestao;
import br.ufpe.cin.fisica.util.Randomizer;

/**
 * EntryPoint para a execucao dos itens (a) e (b).
 *
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class EntryPoint {

    private static final Randomizer RANDOMIZER = Randomizer.getInstance();

    private static final int ITERACOES = 100000;

    /**
     * Executa os itens (a) e (b) e plota os graficos, expostando-os como imagem.
     * 
     * @param args Argumentos necessarios para execucao.
     */
    public static void main(String[] args) {
        // Calcular dados do item (a)
        AbstractQuestao a = new PrimeiraQuestao(RANDOMIZER);
        a.calcular();

        // Calcular dados do item (b)
        AbstractQuestao b = new SegundaQuestao(RANDOMIZER, ITERACOES);
        b.calcular();

        // Plotar graficos para os itens (a) e (b)
        GeradorGrafico geradorGrafico = new GeradorGrafico();
        geradorGrafico.gerar(Letra.A, a.getMapaTempoDistancia());
        geradorGrafico.gerar(Letra.B, b.getMapaTempoDistancia());
    }

}
