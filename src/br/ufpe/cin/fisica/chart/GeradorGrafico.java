package br.ufpe.cin.fisica.chart;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

import br.ufpe.cin.fisica.enums.Letra;
import br.ufpe.cin.fisica.util.Constantes;

/**
 * Criacao de graficos.
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class GeradorGrafico {

    private static final Logger LOGGER = LogManager.getLogger(GeradorGrafico.class.getName());

    private JFreeChart barChart;

    private static final String BAR_LABEL = "Dist�ncia x Tempo";

    private static final int IMAGE_WIDTH = 600;

    private static final int IMAGE_HEIGHT = 480;

    private String nomeArquivo;

    private String tituloGrafico;

    /**
     * Construtor padrao.
     */
    public GeradorGrafico() {
    }

    /**
     * Gera o grafico de acordo com a questao executaada.
     * 
     * @param letra Indica a letra para o qual o grafico sera plotado: (a) ou (b).
     * @param tempoDistancia Mapa com os tempos e distancias calculadas.
     */
    public void gerar(Letra letra, Map<Number, Number> tempoDistancia) {
        // Configura as labels impressas no grafico
        configurarLabels(letra);

        LOGGER.info(String.format("Gerando arquivo %s.", this.nomeArquivo));

        final File imagem = new File(this.nomeArquivo);

        // Cria o objeto que representa o grafico e o configura
        this.barChart = ChartFactory.createBarChart(this.tituloGrafico, "Tempo (s)", "Dist�ncia (m)", criarDataSet(letra, tempoDistancia));
        configurarGrafico(letra);

        try {
            // Exporta o grafico como imagem
            ChartUtilities.saveChartAsJPEG(imagem, this.barChart, IMAGE_WIDTH, IMAGE_HEIGHT);
        } catch (IOException e) {
            LOGGER.error("Erro ao gerar gr�fico!");
        }
    }

    /**
     * Percorre o mapa e cria o conjunto de dados que serao plotados no grafico.
     * 
     * @param letra Indica a letra para o qual o grafico sera plotado: (a) ou (b).
     * @param tempoDistancia Mapa com os tempos e distancias calculadas.
     * @return Dados que serao plotados no grafico.
     */
    private DefaultCategoryDataset criarDataSet(Letra letra, Map<Number, Number> tempoDistancia) {
        final DefaultCategoryDataset dataSet = new DefaultCategoryDataset();

        if (letra == Letra.A) {
            for (Integer tempo = 0; tempo <= tempoDistancia.size(); tempo++) {
                dataSet.addValue(tempoDistancia.get(tempo), BAR_LABEL, tempo);
            }
        } else {
            for (int tempo : Constantes.PASSOS_MAXIMOS) {
                dataSet.addValue(tempoDistancia.get(tempo), BAR_LABEL, tempo);
            }
        }

        return dataSet;
    }

    /**
     * Configura o grafico, setando cor e removendo legenda.
     * 
     * @param letra Indica a letra para o qual o grafico sera plotado: (a) ou (b).
     */
    private void configurarGrafico(Letra letra) {
        final CategoryPlot plot = (CategoryPlot) this.barChart.getPlot();

        plot.getRenderer().setSeriesPaint(0, letra == Letra.A ? Color.RED : Color.BLUE);

        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());

        this.barChart.removeLegend();
    }

    /**
     * Configura as labels que serao impressas no grafico.
     * 
     * @param letra Indica a letra para o qual o grafico sera plotado: (a) ou (b).
     */
    private void configurarLabels(Letra letra) {
        switch (letra) {
            case A:
                this.nomeArquivo = "grafico-a.jpg";
                this.tituloGrafico = "Comportamento err�tico";
                break;
            case B:
                this.nomeArquivo = "grafico-b.jpg";
                this.tituloGrafico = "M�dia de D(j)";
                break;
        }
    }

}
