package br.ufpe.cin.fisica.questoes;

import br.ufpe.cin.fisica.util.Constantes;
import br.ufpe.cin.fisica.util.Randomizer;

/**
 * Resolucao do item (a).
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class PrimeiraQuestao extends AbstractQuestao {

    /**
     * Construtor padrao.
     * 
     * @param randomizer Objeto que define a direcao para a qual a particula ira.
     */
    public PrimeiraQuestao(Randomizer randomizer) {
        super(randomizer);
    }

    /*
     * (non-Javadoc)
     * @see br.ufpe.cin.fisica.questoes.Questao#calcular()
     */
    @Override
    public void calcular() {
        LOGGER.info("Executando item (a)!");

        for (int passo = 1; passo <= Constantes.PASSO_MAXIMO; passo++) {
            this.mapaTempoDistancia.put(passo, Math.abs(this.calcularIndividual(passo)));
        }
    }

}
