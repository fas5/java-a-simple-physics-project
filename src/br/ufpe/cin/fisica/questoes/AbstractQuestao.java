package br.ufpe.cin.fisica.questoes;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.ufpe.cin.fisica.enums.Sentido;
import br.ufpe.cin.fisica.util.Randomizer;
import br.ufpe.cin.fisica.util.Utils;

/**
 * Classe abstrata contendo metodos em comum para a execucao de todos os itens da especificacao do projeto.
 *
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public abstract class AbstractQuestao {

    protected static final Logger LOGGER = LogManager.getLogger(AbstractQuestao.class.getName());

    protected Randomizer randomizer;

    protected Map<Number, Number> mapaTempoDistancia;

    protected int posicao;

    /**
     * Construtor padrao.
     * 
     * @param randomizer Objeto que define a direcao para a qual a particula ira.
     */
    public AbstractQuestao(Randomizer randomizer) {
        this.randomizer = randomizer;
        this.mapaTempoDistancia = new ConcurrentHashMap<>();
        this.posicao = 0;
    }

    /**
     * Inicia a execucao do item.
     */
    public abstract void calcular();

    /**
     * Calcula a distancia final apos j passos.
     * 
     * @param passoMaximo Numero maximo de passos dados.
     * @return A distancia final ate o ponto de origem apos a execucao dos passos.
     */
    protected int calcularIndividual(final int passoMaximo) {
        Sentido sentido;

        this.setPosicao(0);

        for (int passo = 1; passo <= passoMaximo; passo++) {
            sentido = Utils.escolherSentido(this.randomizer);

            // LOGGER.info(String.format("Sentido escolhido: %s.", sentido));

            this.calcularPosicao(sentido);

            // LOGGER.info(String.format("Posicao atual: %s.", getPosicao()));
        }

        return getPosicao();
    }

    /**
     * Calcula a posicao apos um passo no sentido passado como parametro.
     * 
     * @param sentido Sentido para o qual a particula dara o passo.
     * @return Nova distancia a partir da origem apos o passo ser executado.
     */
    protected int calcularPosicao(final Sentido sentido) {
        switch (sentido) {
            case ESQUERDA:
                setPosicao(Utils.decrementarValor(getPosicao()));
                break;
            case DIREITA:
                setPosicao(Utils.incrementarValor(getPosicao()));
                break;
        }

        return getPosicao();
    }

    /**
     * Retorna o valor do campo <CODE>mapaTempoDistancia</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>mapaTempoDistancia</CODE> deste objeto.
     */
    public Map<Number, Number> getMapaTempoDistancia() {
        return this.mapaTempoDistancia;
    }

    /**
     * Retorna o valor do campo <CODE>posicao</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>posicao</CODE> deste objeto.
     */
    public int getPosicao() {
        return this.posicao;
    }

    /**
     * Seta o valor do campo posicao deste objeto.
     *
     * @param posicao valor do campo posicao
     */
    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }

}
