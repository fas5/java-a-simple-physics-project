package br.ufpe.cin.fisica.questoes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.ufpe.cin.fisica.util.Constantes;
import br.ufpe.cin.fisica.util.Randomizer;

/**
 * Resolucao do item (b).
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public class SegundaQuestao extends AbstractQuestao {

    private static final Logger LOGGER = LogManager.getLogger(SegundaQuestao.class.getName());

    private double iteracoes;

    /**
     * Construtor padrao.
     * 
     * @param randomizer Objeto que define a direcao para a qual a particula ira.
     * @param iteracoes Amostragem do experimento. Numero de vezes que o experimento sera executado.
     */
    public SegundaQuestao(Randomizer randomizer, double iteracoes) {
        super(randomizer);

        this.iteracoes = iteracoes;
    }

    /*
     * (non-Javadoc)
     * @see br.ufpe.cin.fisica.questoes.AbstractQuestao#calcular()
     */
    @Override
    public void calcular() {
        LOGGER.info("Executando item (b)!");

        for (int passoMaximo : Constantes.PASSOS_MAXIMOS) {
            calcularMedia(passoMaximo);
        }
    }

    /**
     * Calcula a media das distancias calculadas em cada amostra.
     * 
     * @param passoMaximo Numero de passos maximo dados pela particula.
     */
    private void calcularMedia(final int passoMaximo) {
        double total = 0.0;
        double media;

        for (int i = 1; i <= this.iteracoes; i++) {
            total += Math.abs(this.calcularIndividual(passoMaximo));
        }

        media = Math.abs(total / this.iteracoes);

        LOGGER.info(String.format("Media para t = %d: %f", passoMaximo, media));

        this.mapaTempoDistancia.put(passoMaximo, media);
    }
}
