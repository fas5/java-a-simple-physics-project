package br.ufpe.cin.fisica.enums;

/**
 * Enumera os possiveis sentidos para qual uma particula pode ir.
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public enum Sentido {

    ESQUERDA(0), DIREITA(1);

    private int id;

    /**
     * Construtor padrao.
     * 
     * @param id Identificador do sentido.
     */
    private Sentido(int id) {
        this.id = id;
    }

    /**
     * Retorna o valor do campo <CODE>id</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>id</CODE> deste objeto.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Retorna o sentido de acordo com o id passado como parametro.
     * 
     * @param id Identificador do sentido.
     * @return Sentido representado pelo id passado como parametro.
     */
    public static Sentido getSentidoById(int id) {
        if (id == ESQUERDA.getId()) {
            return ESQUERDA;
        } else {
            return DIREITA;
        }
    }

}
