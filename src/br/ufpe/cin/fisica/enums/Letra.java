package br.ufpe.cin.fisica.enums;

/**
 * Enumera os items requisitados na especifiacao.
 * 
 * @author fas5
 * @since 0.0.1-SNAPSHOT
 */
public enum Letra {

    A, B;

}
