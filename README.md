# Projeto de Física Para Computação - 2016.2 - CIn/UFPE#

## Especificação ##
Considere uma partícula que só pode se mover em uma dimensão, digamos, no eixo x de um sistema de coordenadas. Suponha que inicialmente a partícula está na posição **x = 0**, e que a cada intervalo de tempo fixo **T**, ela pode se deslocar uma certa distância **d** (também fixa) para a direita, com probabilidade **0.5**, ou para a esquerda, com probabilidade **0.5**.

Assim, se em **t = 0**, **x = 0**, então em **t = T** só podemos ter **x = +d** ou **x = -d**. Em **t = 2T**, novamente a partícula pode deslocar-se uma distância d para a direita ou para a esquerda, com as mesmas probabilidades. Então só é possível termos **x = -2d** (passo à esquerda a partir de -d), **x = 0** (passo à esquerda a partir de +d ou passo à direita a partir de -d) e **x = +2d** (passo à direita a partir de +d). Assim sucessivamente, de forma que em **t = jT**, com **j = 1, 2, 3, 4,...**, a partícula deve estar entre **x = -jd** e x=+jd. Por simplicidade assuma que **d = 1 metro** e **T = 1 segundo**.

Faça um programa na linguagem de sua preferência para simular a situação descrita.
* (a) Para uma única realização determine a distância **D** da partícula à origem como função de **j**. Faça um gráfico de **D(j)** para **1 < j < 240**.
* (b) No item (a) você obteve um resultado individual mostrando um comportamento errático. Repita a simulação um número suficiente de vezes **N** e calcule a média de **D(j)** nestas realizações. Faça um gráfico de **D(j)** para **j = 10, 30, 60, 120, 240**. Justifique o número de repetições **N** que você utilizou.
* (c) Faça uma análise sucinta dos seus resultados.